import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Sesion from './Inicio';
import CrearEvento from './Evento';


function App() {

  return (
    <Router>
      <Routes>
        <Route path="/" element={<Sesion />} />
        <Route path="/Evento" element={<CrearEvento/>} />
      </Routes>
    </Router>
  );
}

export default App;
