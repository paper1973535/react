
import React, { useState } from "react";
import { Link } from "react-router-dom";
import picture from './images/img1.jpg';
import './styles.css';

function Sesion() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = (e) => {
    e.preventDefault();
    // Aquí agregarías la lógica de autenticación, por ejemplo, llamando a una API
    // Si la autenticación falla, puedes setear el mensaje de error con setError
  };

  return (
    <div class="fondo">
      <div class="recuadro">
      <div class="image-container">
        <img
          src={picture} // Reemplaza esta ruta con la ruta real de tu imagen
          alt="Imagen de inicio de sesión"
          className="small-image"
        />
        </div>
        <h1 className="iniciosesion">Iniciar Sesión</h1>
        <form onSubmit={handleLogin}>
          <div className="mb-4">
            <label htmlFor="email" className="label">
              Correo Electrónico:
            </label>
            <input
              type="email"
              id="email"
              className="input"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password" className="label">
              Contraseña:
            </label>
            <input
              type="password"
              id="password"
              className="input"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="form-groupbt">
            <Link
              to="/Evento"
              className="button"
            >
              Ingresar
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Sesion;
