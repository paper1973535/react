
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './styles2.css';

function CrearEvento() {
    const [tipoEvento, setTipoEvento] = useState("");

    const [descripcionEvento, setDescripcionEvento] = useState("");
    const [fechaInicio, setFechaInicio] = useState("");
    const [fechaFinalizacion, setFechaFinalizacion] = useState("");
    const [ErrorVacios, setErrorVacios] = useState(false);

    const navigate = useNavigate();

    const generateUniqueId = () => {
        const timestamp = new Date().getTime();
        const randomPart = Math.floor(Math.random() * 10000);
        return `evento_${timestamp}_${randomPart}`;
    };


    const today = new Date();

    const agregarParticipante = () => {
        setParticipante([...participantes, {
            nombrePart: "",
            correoPart: "",
            celPart: "",
            directPart: "",
        }]);
    };


    const agregarCoordinador = () => {
        if (coordinador.length < 3) {
            setCoordinador([...coordinador, {
                nombreCoord: "",
                correoCoord: "",
                telfCoord: "",
                direccCoord: "",
            }]);
        }
    };

    const [participantes, setParticipante] = useState([
        { nombrePart: "", correoPart: "", celPart: "", directPart: "", },
        { nombrePart: "", correoPart: "", celPart: "", directPart: "", }
    ]);

    const [coordinador, setCoordinador] = useState([
        { nombreCoord: "", correoCoord: "", telfCoord: "", direccCoord: "", }
    ]);

    const quitarParticipante = (index) => {
        const nuevosParticipantes = participantes.filter((_, i) => i !== index);
        setParticipante(nuevosParticipantes);

    };

    const quitarCoordinador = (index) => {
        const nuevosCoordinador = coordinador.filter((_, i) => i !== index);
        setCoordinador(nuevosCoordinador);

    };

    const guardarEvento = () => {


        const partValido = () => {
            return participantes.every(particip =>
                particip.nombrePart !== "" && particip.correoPart !== "" && particip.celPart !== "" && particip.directPart !== ""
            );
        };

        const coordVal = () => {
            return coordinador.every(cadacoordinador =>
                cadacoordinador.nombreCoord !== "" && cadacoordinador.correoCoord !== "" && cadacoordinador.telfCoord !== "" && cadacoordinador.direccCoord !== ""
            );
        };

        if (tipoEvento === "" || descripcionEvento === "" || fechaInicio === "" || fechaFinalizacion === "" || participantes.length < 2 || coordinador.length === 3 || !partValido() || !coordVal()) {
            setErrorVacios(true);
            return;
        } else {
            setErrorVacios(false);


            const nuevoEvento = {
                id: generateUniqueId(), // Genera un ID único
                event: tipoEvento,
                coordinador: coordinador,
                descripcion: descripcionEvento,
                fechaInicio: fechaInicio,
                fechaFinalizacion: fechaFinalizacion,
                fechaCreacion: new Date().toLocaleDateString("en-US", {
                    month: "short",
                    day: "numeric",
                    year: "numeric",
                }),
                participantes: participantes,

            };

            console.log(nuevoEvento);
            navigate("/");
        }
    };

    return (
        <div className="fondo2">
            <header className="head">
                <div className="logo">Administrador de Eventos</div>
            </header>
            <div className="spaciovolver">
                <Link
                    to="/"
                    className="botonvolver"
                >
                    Volver
                </Link>
            </div>
            <div>
                <div className="contenedordatosevento">

                    {ErrorVacios && <p>Se requiere llenar toda la información solicitada.</p>}
                    <label className="titulos">Tipo de Evento</label>
                    <select
                        className="tipoopciones"
                        value={tipoEvento}
                        onChange={(e) => setTipoEvento(e.target.value)}
                    >
                        <option value="">Seleccione un Tipo de Evento</option>
                        <option value="Cumpleaños">Cumpleaños</option>
                        <option value="Boda">Boda</option>
                        <option value="Conferencia">Conferencia</option>
                        <option value="Reunión">Reunión</option>
                        <option value="Aniversario">Aniversario</option>
                    </select>

                    <label className="titulos">Datos del Coordinador</label>
                </div>


                <div class="espacioparticipantes">
                    {coordinador.length < 3 && (
                        <button
                            className="agregar"
                            onClick={agregarCoordinador}
                        >
                            Agregar Coordinador
                        </button>
                    )}

                    {coordinador.map((coord, index) => (
                        <div key={index} className="camposjuntos">
                            <input
                                className="camposcoor"
                                type="text"
                                placeholder={`Nombre Completo del Coordinador ${index + 1}`}
                                value={coord.nombreCoord}
                                onChange={(e) => {
                                    const nuevosCoordinador = [...coordinador];
                                    nuevosCoordinador[index].nombreCoord = e.target.value;
                                    setCoordinador(nuevosCoordinador);
                                }}
                            />
                            <input
                                className="camposcoor"
                                type="text"
                                placeholder={`Correo Electrónico del Coordinador ${index + 1}`}
                                value={coord.correoCoord}
                                onChange={(e) => {
                                    const nuevosCoordinador = [...coordinador];
                                    nuevosCoordinador[index].correoCoord = e.target.value;
                                    setCoordinador(nuevosCoordinador);
                                }}
                            />
                            <input
                                className="camposcoor"
                                type="text"
                                placeholder={`Celular del Coordinador ${index + 1}`}
                                value={coord.telfCoord}
                                onChange={(e) => {
                                    const nuevosCoordinador = [...coordinador];
                                    nuevosCoordinador[index].telfCoord = e.target.value;
                                    setCoordinador(nuevosCoordinador);
                                }}
                            />
                            <input
                                className="camposcoor"
                                type="text"
                                placeholder={`Dirección del Coordinador ${index + 1}`}
                                value={coord.direccCoord}
                                onChange={(e) => {
                                    const nuevosCoordinador = [...coordinador];
                                    nuevosCoordinador[index].direccCoord = e.target.value;
                                    setCoordinador(nuevosCoordinador);
                                }}
                            />
                            {coordinador.length > 1 && (
                                <button
                                    className="botoneliminar"
                                    onClick={() => quitarCoordinador(index)}
                                    disabled={coordinador.length === 1}
                                >
                                    Eliminar
                                </button>
                            )}
                        </div>
                    ))}


                    <label className="titulos">Descripción del Evento</label>
                    <input
                        id="descripcion_votacion"
                        className="inputdescrip"
                        type="text"
                        placeholder="Descripción del Evento"
                        value={descripcionEvento}
                        onChange={(e) => setDescripcionEvento(e.target.value)}
                    />

                    <div className="espaciofechas">
                        <div>
                            <label className="titulos">Fecha de Inicio</label>
                            <DatePicker
                                selected={fechaInicio}
                                onChange={date => setFechaInicio(date)}
                                className="fechas"
                                placeholderText="Inicio del Evento"
                                calendarClassName="custom-calendar"
                                minDate={today}
                                maxDate={fechaFinalizacion}
                            />
                        </div>
                        <div>
                            <label className="titulos">Fecha de Finalización</label>
                            <DatePicker
                                selected={fechaFinalizacion}
                                onChange={date => {
                                    // Si la fecha seleccionada es anterior a la fecha de inicio, actualiza la fecha de inicio
                                    if (date < fechaInicio) {
                                        setFechaInicio(date);
                                    }
                                    setFechaFinalizacion(date);
                                }}
                                className="fechas"
                                placeholderText="Fin del Evento"
                                calendarClassName="custom-calendar"
                                minDate={fechaInicio || today} // No permite seleccionar una fecha anterior a la de inicio


                            />
                        </div>
                    </div>
                </div>


            </div>

            <div className="contenedordatosevento">
                <label className="titulos">Participantes del Evento</label>
                <div className="filaagregar">
                    <button
                        className="agregar"
                        onClick={agregarParticipante}
                    >
                        Agregar Participante
                    </button>
                </div>

            </div>

            <div className="espacioparticipantes" >
                {participantes.map((parts, index) => (
                    <div key={index} className="camposjuntos">
                        <input
                            className="camposcoor"
                            type="text"
                            placeholder={`Nombre Completo del Participante ${index + 1}`}
                            value={parts.nombrePart}
                            onChange={(e) => {
                                const nuevosParts = [...participantes];
                                nuevosParts[index].nombrePart = e.target.value;
                                setParticipante(nuevosParts);
                            }}
                        />
                        <input
                            className="camposcoor"
                            type="text"
                            placeholder={`Correo Electrónico del Participante ${index + 1}`}
                            value={parts.correoPart}
                            onChange={(e) => {
                                const nuevosParts = [...participantes];
                                nuevosParts[index].correoPart = e.target.value;
                                setParticipante(nuevosParts);
                            }}
                        />
                        <input
                            className="camposcoor"
                            type="text"
                            placeholder={`Celular del Participante ${index + 1}`}
                            value={parts.directPart}
                            onChange={(e) => {
                                const nuevosParts = [...participantes];
                                nuevosParts[index].directPart = e.target.value;
                                setParticipante(nuevosParts);
                            }}
                        />
                        <input
                            className="camposcoor"
                            type="text"
                            placeholder={`Dirección del Participante ${index + 1}`}
                            value={parts.celPart}
                            onChange={(e) => {
                                const nuevosParts = [...participantes];
                                nuevosParts[index].celPart = e.target.value;
                                setParticipante(nuevosParts);
                            }}
                        />
                        {participantes.length > 2 && (
                            <button

                                className="botoneliminar"
                                onClick={() => quitarParticipante(index)}
                                disabled={participantes.length === 2}
                            >
                                Eliminar
                            </button>
                        )}
                    </div>
                ))}

            </div>

            <div className="espacioguardar">
                <button
                    className="botonguardar"
                    onClick={guardarEvento}
                >
                    Guardar Evento
                </button>

            </div>
        </div>
    );
}

export default CrearEvento;